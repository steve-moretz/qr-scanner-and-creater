import React, {useEffect, useState} from 'react';
import {Dimensions, FlatList, StyleSheet, Text, View} from 'react-native';
import {Button, Card, TextInput} from "react-native-paper";
import QRCode from "react-native-qrcode";
import {useDispatch, useSelector} from "react-redux";
import {BarCodeScanner} from "expo-barcode-scanner";
import {addAllQrCodes, addQr} from "../store/qrAction";
import QRCodeEntity from "../db/entities/QRCodeEntity";

const MainScreen = props => {
    const [hasPermission, setHasPermission] = useState(null);
    const [lastScannedBarcode, setLastScannedBarcode] = useState();
    const [text, setText] = useState('');

    const qrList = useSelector(state => state.qrCodes.list);

    const askForPermission = async () => {
        setHasPermission((await BarCodeScanner.requestPermissionsAsync()).status === 'granted');
    };

    const Item = (p) => {
        return (
            <Button onPress={() => {
                setText(p.text)
            }}>{p.text}</Button>
        )
    };

    const dispatch = useDispatch();

    useEffect(() => {
        (async () => {
            await askForPermission();
            // await QRCodeEntity.createTable();
            // QRCodeEntity.destroyAll();
            const options = {
                order: 'timestamp DESC'
            };
            var res = await QRCodeEntity.query(options);
            if (!res) {res = []}
            dispatch(addAllQrCodes(res.map((item) => {return item.qrCode})));
        })();
    }, []);

    useEffect(() => {
        (async ()=>{
            if (lastScannedBarcode) {
                try {
                    const res = await new QRCodeEntity({qrCode: lastScannedBarcode}).save();
                }catch (e) {
                    console.log(e);
                }
                dispatch(addQr(lastScannedBarcode));
                setText(lastScannedBarcode);
                // console.log(lastScannedBarcode);
            }
        })()
    }, [lastScannedBarcode]);


    const barCodeScannedHandler = ({data}) => {
        setLastScannedBarcode(data);
    };

    return (
        <View style={styles.container}>
            <TextInput style={{marginTop: 40, width: 300, maxWidth: '80%'}} value={text} onChangeText={setText}
                       onBlur={() => {
                           setLastScannedBarcode(text)
                       }}/>
            {
                qrList.length !== 0 && (
                    <Card style={{marginBottom: 300, marginVertical: 20, width: '80%', maxWidth: '80%'}}>
                        <Card.Content>
                            <FlatList data={qrList} inverted keyExtractor={() => {
                                return Math.random().toString()
                            }} renderItem={({item}) => {
                                return <Item text={item}/>
                            }}/>
                        </Card.Content>
                    </Card>
                )
            }
            <View style={{
                width: Dimensions.get('screen').width / 2, flexDirection: 'column',
                position: "absolute",
                top: Dimensions.get('screen').height - 200,
                bottom: 0,
                right: 0,
                left: Dimensions.get('screen').width - 170,
                zIndex: 2
            }}>
                <QRCode
                    value={text}
                    size={Dimensions.get('screen').width / 2}
                    bgColor='skyblue'
                    fgColor='white'/>
            </View>
            {hasPermission === null && <Text>Requesting Camera Permission</Text>}
            {hasPermission === false && <Button onPress={askForPermission} title='Give Access To Camera'/>}
            {hasPermission === true && (
                <BarCodeScanner
                    onBarCodeScanned={barCodeScannedHandler}
                    style={{flex: 1, width: Dimensions.get('screen').width / 2, height: 500,
                        position: "absolute",
                        top: Dimensions.get('screen').height - 290,
                        bottom: 0,
                        right: 0,
                        left: 0,
                        zIndex: 2}}
                />
            )}
        </View>
    )
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'flex-start',
        flexDirection: 'column',
    },
});


export default MainScreen;
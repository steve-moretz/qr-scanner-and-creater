import * as SQLite from 'expo-sqlite';
import { BaseModel, types } from 'expo-sqlite-orm'

export default class QRCodeEntity extends BaseModel {
    constructor(obj) {
        super(obj)
    }

    static get database() {
        return async () => SQLite.openDatabase('database.db')
    }

    static get tableName() {
        return 'qrCodes'
    }

    static get columnMapping() {
        return {
            qrCode: { type: types.TEXT },
            timestamp: { type: types.INTEGER, default: () => Date.now() }
        }
    }
}
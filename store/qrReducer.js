import {ADD_ALL_QR_CODES, ADD_QR} from "./qrAction";

const initialState = {
    list : []
};

export default (state = initialState,action) => {
    switch (action.type) {
        case ADD_QR:
            return {...state,list : [action.text,...state.list]};
        case ADD_ALL_QR_CODES:
            return {...state,list : action.qrCodes};
    }
    return state;
}